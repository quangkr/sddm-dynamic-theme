# Dynamic SDDM theme
Modified version of Adapta theme from [SDDM-Dynamic-Themes](https://github.com/Rokin05/SDDM-Themes)

## Credits
* Light theme background: Photo by [Chris Brignola](https://unsplash.com/@cjbrignola) on [Unsplash](https://unsplash.com)
